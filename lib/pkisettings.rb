#!/usr/bin/env ruby

require 'yaml'

class PKISettings
  def initialize(filepath)
    @file=filepath
    @settings=self.load
  end
  def create dn
    raise "#{@file} already exists" if File.exists?(@file)
    @dn = dn
    default = {
      "serial" => 1,
      "crlnumber" => 1,
      "DN" => dn
    }
    write default
  end

  def load
    YAML::load_file @file if File.exists?(@file)
  end

  def serial
    @settings['serial']
  end

  def serial!
    @settings['serial'] += 1
    write
    @settings['serial']
  end

  def write settings=@settings
    begin
      file = File.open(@file, "w")
      file.write settings.to_yaml
    rescue Errno::ENOENT => e
      puts "Could not open file"
      puts e.message
    rescue IOError => e
      puts "Could not write to file"
      puts e.message
    ensure
      file.close unless file == nil
    end
  end

  # -- Class methods

  def self.dn_fields
    return [
      ['C',            'Country Name (2 letter code)', 'US'],
      ['ST',           'Abbreviated State Name',       'CA'],
      ['L',            'Locality Name (eg, city)',     'San Francisco'],
      ['O',            'Organization Name',            'Your Company'],
      ['OU',           'Organization Unit Name',       'Your Company Certificate Issuer'],
      ['emailAddress', 'Email Address',                'name@domain.com'],
    ]
  end
end
