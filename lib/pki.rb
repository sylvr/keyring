#!/usr/bin/env ruby
#
require 'fileutils'
require 'openssl'
require 'pkisettings'

CONFIG_NAME='pkiconfig.yml'

class Hash
  def flatten
    map {|e| e.join('=')}.join('/')
  end
end

class PKI

  def initialize path
    @path = path
    @settings = PKISettings.new "#{path}/#{CONFIG_NAME}"
    @dn = @settings.load['DN']
  end

  def new_key name, size, encrypt=false
    key = OpenSSL::PKey::RSA.new size
    if encrypt
      chmod = 0400
      cipher = OpenSSL::Cipher::Cipher.new 'AES-128-CBC'
      #TODO: real password interaction typing and verification
      print "Enter a passphrase : "
      private_key = key.export(cipher, $stdin.gets.chomp())
    end
    self.class.store private_key ||= key, "#{@path}/keys/private/#{name}.key", chmod
    self.class.store key.public_key, "#{@path}/keys/public/#{name}.pub.key"
    return key
  end

  def gen_dh size=2048
    dh = OpenSSL::PKey::DH.new(size)
    self.class.store dh.to_pem, "#{@path}/certs/dh#{size}.pem"
  end

  def gen_csr cn, key, name
    @dn['CN'] = cn
    csr = OpenSSL::X509::Request.new
    csr.version = 0
    csr.subject = OpenSSL::X509::Name.parse @dn.flatten
    csr.public_key = key.public_key
    csr.sign key, OpenSSL::Digest::SHA1.new
    self.class.store csr.to_pem, "#{@path}/csr/#{name}.pem"
    return csr
  end

  def check_csr request
    csr = OpenSSL::X509::Request.new request
    raise 'CSR can not be verified' unless csr.verify csr.public_key
  end

  def sign_csr name, csr, key, type, issuer=nil, issuer_key=nil, validity=1
    year=31536000
    cert = OpenSSL::X509::Certificate.new
    #TODO: version and serial must be dynamic
    cert.version = 0
    cert.serial = @settings.serial!
    cert.subject = csr.subject
    cert.issuer = issuer ? issuer.subject : csr.subject
    cert.public_key = csr.public_key
    cert.not_before = Time.now
    cert.not_after = cert.not_before + validity * year
    ef = OpenSSL::X509::ExtensionFactory.new
    ef.subject_certificate = cert
    ef.issuer_certificate = issuer ? issuer : cert
    case type
    when :ca
      cert.add_extension ef.create_extension 'subjectKeyIdentifier', 'hash'
      cert.add_extension ef.create_extension 'authorityKeyIdentifier', 'keyid:always, issuer:always'
      cert.add_extension ef.create_extension 'basicConstraints', 'CA:TRUE', true
      cert.add_extension ef.create_extension 'keyUsage', 'digitalSignature, cRLSign, keyCertSign', true
      cert.add_extension ef.create_extension 'nsCertType', 'sslCA, emailCA'
    when :server
      cert.add_extension ef.create_extension 'subjectKeyIdentifier', 'hash'
      cert.add_extension ef.create_extension 'authorityKeyIdentifier', 'keyid, issuer'
      cert.add_extension ef.create_extension 'basicConstraints', 'CA:FALSE', true
      cert.add_extension ef.create_extension 'keyUsage', 'digitalSignature, keyEncipherment', true
      cert.add_extension ef.create_extension 'nsCertType', 'server'
      cert.add_extension ef.create_extension 'extendedKeyUsage', 'serverAuth'
    when :client
      cert.add_extension ef.create_extension 'subjectKeyIdentifier', 'hash'
      cert.add_extension ef.create_extension 'authorityKeyIdentifier', 'keyid, issuer'
      cert.add_extension ef.create_extension 'basicConstraints', 'CA:FALSE', true
      cert.add_extension ef.create_extension 'keyUsage', 'digitalSignature, keyEncipherment', true
      cert.add_extension ef.create_extension 'nsCertType', 'client'
      cert.add_extension ef.create_extension 'extendedKeyUsage', 'clientAuth'
    when :email
      cert.add_extension ef.create_extension 'subjectKeyIdentifier', 'hash'
      cert.add_extension ef.create_extension 'authorityKeyIdentifier', 'keyid, issuer'
      cert.add_extension ef.create_extension 'basicConstraints', 'CA:FALSE', true
      cert.add_extension ef.create_extension 'keyUsage', 'digitalSignature, keyEncipherment', true
      cert.add_extension ef.create_extension 'extendedKeyUsage', 'emailProtection'
    else
      raise "certificate type #{type} unknown. Unable to sign it"
    end
    cert.sign issuer_key ? issuer_key : key, OpenSSL::Digest::SHA256.new
    self.class.store cert.to_pem, "#{@path}/certs/#{name}.pem"
    return cert
  end

  def self.store data, path, chmod=0644
    #TODO raise error if path already exists
    begin
      file = File.open path, 'w', chmod
      file.write data
    rescue Errno::ENOENT => e
        puts "Could not open file"
        puts e.message
    rescue IOError => e
        puts "Could not write to file"
        puts e.message
    ensure
      file.close unless file == nil
    end
  end
end
