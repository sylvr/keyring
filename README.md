# Keyring, PKI store manager

This (yet another one) PKI Store manager was first designed to help me learn Ruby
while working on a useful tool tailored for my needs.  
It's not a very user friendly tool atm but I intend to improve and finish it on my free time

## Usage

### Initialize a keystore

You just need to use the init command from the tool to generate an empty store

```
$ keyring init -p keystore
Creating PKI filesystem ..."[OK]"
Country Name (2 letter code) (US) :
Abbreviated State Name (CA) :
Locality Name (eg, city) (San Francisco) :
Organization Name (Your Company) :
Organization Unit Name (Your Company Certificate Issuer) :
Email Address (name@domain.com) :
Generating Diffie-Hellman key ...[OK]
Root Certificate Common Name (Root CA) :
Enter a passphrase : ******
Store successfully created
```

As you can see, it generates the settings with all common fields in [keystore]/pkiconfig.yml which you
can modify later if needed.  
It also generates a DH key and a self-signed Root CA.

### keystore tree

The keystore contains all the certificates, keys and csr you will issue.
It is organized regarding that :

```
keystore/
├── certs
│   ├── dh2048.pem
│   └── root_ca.pem
├── csr
│   └── root_ca.pem
├── keys
│   ├── private
│   │   └── root_ca.key
│   └── public
│       └── root_ca.pub.key
├── pkiconfig.yml
└── revoked
```

### Using your keystore

You have 2 ways to manage your keystore, use the keyring tool from the root path of your keystore
or invoke the tool from anywhere with the -p option pointing to the keystore you want to use.

### generating a CSR

There's a convenient method to generate easily a csr, simply give it a name :

```
$ keyring gen_csr -n intermediate_ca
Enter the certificate Common Name (CN) : Intermediate Authority
Enter a passphrase : *****
A new csr has been generated in keystore/csr/intermediate_ca.pem
```

### signing a csr

Next step we need to sign our csr, to do so the sign method will do that for you.

Give it the name of the certificate you want to sign (it will be automatically found in the keystore), the issuer certificate
and which type you want your certificate to become : ca, server, client or email.  
You will be ask passphrase for both your csr key and the issuer key in order to sign it
This one is an intermediate ca, so be it :

```
$ keyring sign -n intermediate_ca -i root_ca -t ca
Enter your csr key pass phrase: *****
Enter the issuer key pass phrase: *****
Your Certificate has been successfully signed in keystore/certs/intermediate_ca.pem
```

##TODO

A lot of things need still to be done :

- Random serialization
- CRL management
- Revocation management
- Improve code and verobosity
- Better errors handling
- Add some nice colorized output à la homebrew
- Become a Gem
- Create a brew for it
- ...

